from django.contrib import admin
from employee_attrition.models import EmployeeAttrition

class EmployeeAttritionAdmin( admin.ModelAdmin ):
    list_display = ( 'EmployeeNumber', 'Department', 'EducationField', 'JobRole' )

admin.site.register( EmployeeAttrition, EmployeeAttritionAdmin )