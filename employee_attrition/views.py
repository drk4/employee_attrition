from django.shortcuts import render
from django.http import JsonResponse
from django.core.paginator import Paginator
import json
from employee_attrition.models import EmployeeAttrition


PAGE_SIZE = 100


def invalidRequest():
    return JsonResponse({
            'ok': False,
            'items': []
        }, status= 400 )


def home( request ):
    return render( request, 'index.html' )


def getAll( pageNumber ):
    items = EmployeeAttrition.objects.all().order_by( 'pk' )

    paginator = Paginator( items, PAGE_SIZE )
    page = paginator.get_page( pageNumber )
    serializableItems = list( page.object_list.values() )

    return JsonResponse({
        'ok': True,
        'items': serializableItems,
        'isLast': not page.has_next()
    })


def getFiltered( propNames, values, pageNumber ):
    try:
        filterArg = {}

        for prop, value in zip( propNames, values ):
            filterArg[ prop + '__iexact' ] = value

        items = EmployeeAttrition.objects.filter( **filterArg ).order_by( 'pk' ).values()

    except:
        return invalidRequest()

    paginator = Paginator( items, PAGE_SIZE )
    page = paginator.get_page( pageNumber )
    serializableItems = list( page.object_list.values() )

    return JsonResponse({
        'ok': True,
        'items': serializableItems,
        'isLast': not page.has_next()
    })


def search( request ):
    try:
        body = json.loads( request.body )
        propNames = body[ 'propNames' ]
        values = body[ 'values' ]
        pageNumber = body[ 'page' ]

    except:
        return invalidRequest()

    propLength = len( propNames )
    valuesLength = len( values )

    if propLength != valuesLength:
        return invalidRequest()

        # apply no filter
    if propLength == 0:
        return getAll( pageNumber )

    else:
        return getFiltered( propNames, values, pageNumber )

