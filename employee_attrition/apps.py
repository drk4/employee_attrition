from django.apps import AppConfig


class EmployeeAttritionConfig(AppConfig):
    name = 'employee_attrition'
