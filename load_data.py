# run: python manage.py shell
# then inside the shell: exec( open( 'load_data.py' ).read() )

import csv
from employee_attrition.models import EmployeeAttrition

print( '\nLoading CSV data.\n' )

with open( 'data.csv', newline='\n' ) as csvFile:
    reader = csv.reader( csvFile, delimiter= ',' )
    for i, row in enumerate( reader ):

            # there's some empty lines, just move along
        if len( row ) == 0:
            continue

        try:
            employee = EmployeeAttrition.objects.create(
                Age= row[ 0 ],
                Attrition= row[ 1 ],
                BusinessTravel= row[ 2 ],
                DailyRate= row[ 3 ],
                Department= row[ 4 ],
                DistanceFromHome= row[ 5 ],
                Education= row[ 6 ],
                EducationField= row[ 7 ],
                EmployeeCount= row[ 8 ],
                EmployeeNumber= row[ 9 ],
                EnvironmentSatisfaction= row[ 10 ],
                Gender= row[ 11 ],
                HourlyRate= row[ 12 ],
                JobInvolvement= row[ 13 ],
                JobLevel= row[ 14 ],
                JobRole= row[ 15 ],
                JobSatisfaction= row[ 16 ],
                MaritalStatus= row[ 17 ],
                MonthlyIncome= row[ 18 ],
                MonthlyRate= row[ 19 ],
                NumCompaniesWorked= row[ 20 ],
                Over18= row[ 21 ],
                OverTime= row[ 22 ],
                PercentSalaryHike= row[ 23 ],
                PerformanceRating= row[ 24 ],
                RelationshipSatisfaction= row[ 25 ],
                StandardHours= row[ 26 ],
                StockOptionLevel= row[ 27 ],
                TotalWorkingYears= row[ 28 ],
                TrainingTimesLastYear= row[ 29 ],
                WorkLifeBalance= row[ 30 ],
                YearsAtCompany= row[ 31 ],
                YearsInCurrentRole= row[ 32 ],
                YearsSinceLastPromotion= row[ 33 ],
                YearsWithCurrManager= row[ 34 ],
            )
        except BaseException as e:
            print( 'Problem on line:', i, '-', str( e ) )
