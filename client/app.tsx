import * as React from "react";
import * as Cookie from "js-cookie";
import Entry from "./entry";
import Help from "./help";
import DataTable from "./data_table";
import { Data } from "./index";
import Graph from "./graph";

enum ShowResult {
    table, graph
}

interface PropValuePair {
    page: number;       // the data is separated by pages, this has the latest page number that was loaded (for the given options)
    propNames: string[];
    values: string[];
}

interface ResponseData {
    'ok': boolean;
    items: Data[];
    isLast: boolean;    // if its the last available page (no more items)
}

interface AppProps {

}

interface AppState {
    data: Data[];
    previousSearch: string | undefined;
    message: string;
    show: ShowResult;
    isLastPage: boolean;
    postData: PropValuePair;
}

class App extends React.Component <AppProps, AppState> {

    constructor( props: AppProps ) {
        super( props );
        this.search = this.search.bind( this );
        this.showTable = this.showTable.bind( this );
        this.showGraph = this.showGraph.bind( this );
        this.loadMoreItems = this.loadMoreItems.bind( this );

        this.state = {
            data: [],
            previousSearch: undefined,
            message: '',
            show: ShowResult.table,
            isLastPage: false,
            postData: { page: 1, propNames: [], values: [] }
        };
    }

    componentDidMount() {
        this.search( '' );
    }

    /**
     * Filter the results based on a property/value.
     * An empty query means to get all the items (without applying any filter).
     */
    async search( query: string ) {

        if ( query === this.state.previousSearch ) {
            return;
        }

        this.setState({
            message: 'Loading...'
        });

            // if the query is empty we don't apply a filter, so the 'propNames' will be an empty array
        let postData = this.getPropValuePairs( query );
        let data = await this.makeRequest( postData );

        if ( data ) {
            if ( !data.ok ) {
                this.setState({
                    message: `"${ query }" - Property not found.`
                });
                return;
            }

            this.setState({
                data: data.items,
                previousSearch: query,
                message: '',
                isLastPage: data.isLast,
                postData: postData
            });
        }
    }

    /**
     * We don't load all the items at once. This will request more data from the server (the next page).
     */
    async loadMoreItems() {
        this.setState({
            message: 'Loading...'
        });

        var nextPage = Object.assign( {}, this.state.postData );
        nextPage.page++;

        var data = await this.makeRequest( nextPage );

        if ( data && data.ok ) {
            this.setState({
                data: this.state.data.concat( data.items ),
                postData: nextPage,
                isLastPage: data.isLast,
                message: ''
            });
        }
    }

    /**
     * Make a request to the server.
     * Make sure we send the csrf token.
     */
    async makeRequest( requestData: PropValuePair ): Promise <ResponseData | undefined> {
        var headers = new Headers();
        headers.append( 'X-CSRFToken', Cookie.get( 'csrftoken' )! );

        try {
            var response = await fetch( '/search', {
                method: 'POST',
                credentials: 'include',
                headers: headers,
                body: JSON.stringify( requestData )
            });
        }

        catch( error ) {
            this.setState({
                message: 'Failed to connect to the server. Try again.'
            });
            return;
        }

        return await response.json();
    }

    /**
     * Get the property/value pairs.
     * The query string comes in the format: "prop1:value1 prop2:value2 (etc)"
     */
    getPropValuePairs( query: string ) {

            // split on whitespace to get the pairs
            // but only when its followed by a word and colon
        let pairs = query.split( /\s+(?=[\w]+\:)/g );
        let result: PropValuePair = {
            propNames: [],
            values: [],
            page: 1
        };

        for (let a = 0 ; a < pairs.length ; a++) {
            let pair = pairs[ a ];
            let split = pair.split( ':' );

                // not a valid format
            if ( split.length < 2 ) {
                continue;
            }

            let propName = split[ 0 ];
            let value = split[ 1 ];

                // add the pair
            result.propNames.push( propName );
            result.values.push( value );
        }

        return result;
    }

    /**
     * Show the data in a table.
     */
    showTable() {
        this.setState({
            show: ShowResult.table
        });
    }

    /**
     * Show the data in a graph.
     */
    showGraph() {
        this.setState({
            show: ShowResult.graph
        });
    }

    render() {
        let displayResults;

        if ( this.state.data.length > 0 ) {

            let result;
            let tableCssClass = 'button';
            let graphCssClass = 'button';

            switch( this.state.show ) {
                case ShowResult.graph:
                    result = <Graph data= { this.state.data } />;
                    graphCssClass += ' selected';
                    break;

                case ShowResult.table:
                    result = <DataTable data= { this.state.data } filteredProps= { this.state.postData.propNames } />;
                    tableCssClass += ' selected';
                    break;
            }

            displayResults = (
                <div>
                    <ul className="buttonList">
                        <li className= { tableCssClass } onClick= { this.showTable }>Table</li>
                        <li className= { graphCssClass } onClick= { this.showGraph }>Graph</li>
                    </ul>
                    { result }
                    {
                        !this.state.isLastPage ?
                        <div className="button" onClick= { this.loadMoreItems }>Load more items</div> :
                        undefined
                    }
                </div>
            );
        }

        return (
            <div>
                {
                    this.state.message ?
                    <div id="Message">{ this.state.message }</div> :
                    undefined
                }
                <Help />
                <Entry onEnterPress= { this.search } />
                <div>Current Filter: { this.state.previousSearch ? `"${ this.state.previousSearch }"` : '---' }</div>
                { displayResults }
            </div>
        );
    }
}

export default App;