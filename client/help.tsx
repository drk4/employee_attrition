import * as React from 'react';

class Help extends React.Component {
    render() {
        return (
            <div id="Help">
                <p>Use the entry to filter the data. An empty search shows all the rows.</p>
                <p>Format: <em>"Property:Value"</em>, where the property is the column header (without spaces).</p>
                <p>For example: <em>"Attrition:Yes"</em> --- <em>"Education:2 JobRole:Research Scientist"</em></p>
            </div>
        );
    }
}

export default Help;