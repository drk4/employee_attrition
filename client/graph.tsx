import * as React from "react";
import { Data, DataKeys } from "./index";

interface IndexedDict {
    [key: string]: number;
}

declare class Chart {
    constructor( ctx: CanvasRenderingContext2D, options: Object );
    destroy(): void;
}

enum SelectedChart {
    doughnut, bar
}

interface ChartDataSet {
    label: string;
    data: number[];
    hidden: boolean;
    backgroundColor: string[];
}

interface GraphProps {
    data: Data[];
}

interface GraphState {
    selected: SelectedChart;
}

class Graph extends React.Component <GraphProps, GraphState> {

    charts: Chart[] = [];

    constructor( props: GraphProps ) {
        super( props );
        this.showBarChart = this.showBarChart.bind( this );
        this.showDoughnutChart = this.showDoughnutChart.bind( this );

        this.state = {
            selected: SelectedChart.bar
        };
    }

    /**
     * Draw the chart based on the given data.
     */
    draw( data: Data[] ) {

        if ( data.length === 0 ) {
            return;
        }

            // build the data-sets
            // for the bar type
        let labels = [];
        let ages: ChartDataSet = {
            label: 'Age',
            data: [],
            hidden: false,
            backgroundColor: Array( data.length ).fill( 'red' ),
        };
        let monthlyIncome: ChartDataSet = {
            label: 'Monthly Income',
            data: [],
            hidden: true,
            backgroundColor: Array( data.length ).fill( 'blue' ),
        };
        let distanceFromHome: ChartDataSet = {
            label: 'Distance From Home',
            data: [],
            hidden: true,
            backgroundColor: Array( data.length ).fill( 'green' )
        };
        let education: ChartDataSet = {
            label: 'Education',
            data: [],
            hidden: true,
            backgroundColor: Array( data.length ).fill( 'purple' )
        };

            // for the doughnut type
        let gender: IndexedDict = {
            Male: 0,
            Female: 0
        };
        let department: IndexedDict = {
            Sales: 0,
            "Research & Development": 0,
            "Human Resources": 0
        };
        let educationField: IndexedDict = {
            "Life Sciences": 0,
            "Other": 0,
            "Medical": 0,
            "Marketing": 0,
            "Technical Degree": 0
        };
        let jobRole: IndexedDict = {
            "Sales Executive": 0,
            "Research Scientist": 0,
            "Laboratory Technician": 0,
            "Manufacturing Director": 0,
            "Healthcare Representative": 0
        };

        for (let a = 0 ; a < data.length ; a++) {
            let info = data[ a ];

            labels.push( info.EmployeeNumber );

            ages.data.push( info.Age );
            monthlyIncome.data.push( info.MonthlyIncome );
            distanceFromHome.data.push( info.DistanceFromHome );
            education.data.push( info.Education );

            gender[ info.Gender ]++;
            department[ info.Department ]++;
            educationField[ info.EducationField ]++;
            jobRole[ info.JobRole ]++;
        }

            // draw the bar chart
        let barCanvas = this.refs.barChart as HTMLCanvasElement;
        let barCtx = barCanvas.getContext( '2d' )!;

        var barChart = new Chart( barCtx, {
            type: 'bar',
            data: {
                labels: labels,
                datasets: [
                    ages, monthlyIncome, distanceFromHome, education
                ]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
        this.charts.push( barChart );

            // draw the gender chart
        let genderCanvas = this.refs.genderChart as HTMLCanvasElement;
        let genderCtx = genderCanvas.getContext( '2d' )!;

        var genderChart = new Chart( genderCtx, {
            type: 'doughnut',
            data: {
                labels: [ 'Male', 'Female' ],
                datasets: [
                    {
                        label: 'Gender',
                        data: [ gender.Male, gender.Female ],
                        backgroundColor: [ 'blue', 'red' ]
                    }
                ]
            }
        });
        this.charts.push( genderChart );

            // draw the department chart
        let departmentCanvas = this.refs.departmentChart as HTMLCanvasElement;
        let departmentCtx = departmentCanvas.getContext( '2d' )!;

        var departmentChart = new Chart( departmentCtx, {
            type: 'doughnut',
            data: {
                labels: [ 'Sales', 'Research & Development', 'Human Resources' ],
                datasets: [
                    {
                        label: 'Department',
                        data: [ department[ 'Sales' ], department[ 'Research & Development'], department[ 'Human Resources' ] ],
                        backgroundColor: [ 'blue', 'red', 'green' ]
                    }
                ]
            }
        });
        this.charts.push( departmentChart );

            // draw the education field chart
        let educationFieldCanvas = this.refs.educationFieldChart as HTMLCanvasElement;
        let educationFieldCtx = educationFieldCanvas.getContext( '2d' )!;

        var educationFieldChart = new Chart( educationFieldCtx, {
            type: 'doughnut',
            data: {
                labels: [ 'Life Sciences', 'Other', 'Medical', 'Marketing', 'Technical Degree' ],
                datasets: [
                    {
                        label: 'Education Field',
                        data: [
                            educationField[ 'Life Sciences' ],
                            educationField[ 'Other' ],
                            educationField[ 'Medical' ],
                            educationField[ 'Marketing' ],
                            educationField[ 'Technical Degree' ]
                        ],
                        backgroundColor: [ 'blue', 'red', 'green', 'purple', 'gray' ]
                    }
                ]
            }
        });
        this.charts.push( educationFieldChart );

            // draw the job role chart
        let jobRoleCanvas = this.refs.jobRoleChart as HTMLCanvasElement;
        let jobRoleCtx = jobRoleCanvas.getContext( '2d' )!;

        var jobRoleChart = new Chart( jobRoleCtx, {
            type: 'doughnut',
            data: {
                labels: [ 'Sales Executive', 'Research Scientist', "Laboratory Technician", "Manufacturing Director", "Healthcare Representative" ],
                datasets: [
                    {
                        label: 'Department',
                        data: [
                            jobRole[ 'Sales Executive' ],
                            jobRole[ 'Research Scientist'],
                            jobRole[ 'Laboratory Technician' ],
                            jobRole[ 'Manufacturing Director' ],
                            jobRole[ 'Healthcare Representative' ],
                        ],
                        backgroundColor: [ 'blue', 'red', 'green', 'purple', 'gray' ]
                    }
                ]
            }
        });
        this.charts.push( jobRoleChart );
    }

    clear() {
        for (let a = 0 ; a < this.charts.length ; a++) {
            this.charts[ a ].destroy();
        }

        this.charts.length = 0;
    }

    componentDidMount() {
        this.draw( this.props.data );
    }

    componentWillReceiveProps( nextProps: GraphProps ) {

        if ( this.props.data !== nextProps.data ) {
            this.clear();
            this.draw( nextProps.data );
        }
    }

    showDoughnutChart() {
        this.setState({
            selected: SelectedChart.doughnut
        });
    }

    showBarChart() {
        this.setState({
            selected: SelectedChart.bar
        })
    }

    render() {
            // the css classes of the html elements
        let doughnutButton = 'button';
        let barButton = 'button';

        let doughnutCanvas = '';
        let barCanvas = '';

        switch( this.state.selected ) {
            case SelectedChart.doughnut:
                barCanvas += ' hidden'; // hide the other canvas
                doughnutButton += ' selected';
                break;

            case SelectedChart.bar:
                doughnutCanvas += ' hidden';    // hide the other canvas
                barButton += ' selected';
                break;
        }

        return (
            <div>
                <ul className="buttonList">
                    <li className= { barButton } onClick= { this.showBarChart }>Bar</li>
                    <li className= { doughnutButton } onClick= { this.showDoughnutChart }>Doughnut</li>
                </ul>
                <canvas ref="barChart" className= { barCanvas }></canvas>
                <div className= { doughnutCanvas }>
                    <div className="sideBySide">
                        <h2>Gender</h2>
                        <canvas ref="genderChart"></canvas>
                    </div>
                    <div className="sideBySide">
                        <h2>Department</h2>
                        <canvas ref="departmentChart"></canvas>
                    </div>
                    <div className="sideBySide">
                        <h2>Education Field</h2>
                        <canvas ref="educationFieldChart"></canvas>
                    </div>
                    <div className="sideBySide">
                        <h2>Job Role</h2>
                        <canvas ref="jobRoleChart"></canvas>
                    </div>
                </div>
            </div>
        );
    }
}

export default Graph;