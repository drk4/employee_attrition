import * as React from "react";
import { Data, DataKeys } from "./index";

interface DataTableProps {
    data: Data[];
    filteredProps: string[];
}

interface DataTableState {

}

class DataTable extends React.Component <DataTableProps, DataTableState> {

    constructor( props: DataTableProps ) {
        super( props );
    }

    render() {

        let rows = [];
        let data = this.props.data;
        let length = data.length;

            // columns to highlight (the ones that were filtered)
        let highlight = [];

            // find the column positions of the filtered columns
        for (let a = 0 ; a < this.props.filteredProps.length ; a++) {
            let prop = this.props.filteredProps[ a ];
            let position = DataKeys.indexOf( prop );
            highlight.push( position );
        }

            // only render the table if there's some data to be displayed
        if ( length === 0 ) {
            return (
                <div id="DataTable">Empty table.</div>
            );
        }

        for (let a = 0 ; a < length ; a++) {
            let info = data[ a ];
            let dataElements = [];

            for (let a = 0 ; a < DataKeys.length ; a++) {

                    // style differently the filtered columns
                let tdCss;
                if ( highlight.includes( a ) ) {
                    tdCss = 'filtered';
                }

                let key = DataKeys[ a ];
                dataElements.push(
                    <td key= { a } className= { tdCss }>{ info[ key ] }</td>
                );
            }

            rows.push( <tr key= { a }>{ dataElements }</tr> );
        }

        return (
            <div>
                <div id="DataTableContainer">
                    <table id="DataTable">
                        <thead>
                            <tr>
                                <th>Age</th>
                                <th>Attrition</th>
                                <th>Business Travel</th>
                                <th>Daily Rate</th>
                                <th>Department</th>
                                <th>Distance From Home</th>
                                <th>Education</th>
                                <th>Education Field</th>
                                <th>Employee Count</th>
                                <th>Employee Number</th>
                                <th>Environment Satisfaction</th>
                                <th>Gender</th>
                                <th>Hourly Rate</th>
                                <th>Job Involvement</th>
                                <th>Job Level</th>
                                <th>Job Role</th>
                                <th>Job Satisfaction</th>
                                <th>Marital Status</th>
                                <th>Monthly Income</th>
                                <th>Monthly Rate</th>
                                <th>Num Companies Worked</th>
                                <th>Over 18</th>
                                <th>Over Time</th>
                                <th>Percent Salary Hike</th>
                                <th>Performance Rating</th>
                                <th>Relationship Satisfaction</th>
                                <th>Standard Hours</th>
                                <th>Stock Option Level</th>
                                <th>Total Working Years</th>
                                <th>Training Times Last Year</th>
                                <th>Work Life Balance</th>
                                <th>Years At Company</th>
                                <th>Years In Current Role</th>
                                <th>Years Since Last Promotion</th>
                                <th>Years With Curr Manager</th>
                            </tr>
                        </thead>
                        <tbody>
                            { rows }
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default DataTable;