import * as React from "react";
import * as ReactDOM from "react-dom";
import App from "./app";

export const DataKeys = [
    'Age', 'Attrition', 'BusinessTravel', 'DailyRate', 'Department', 'DistanceFromHome', 'Education', 'EducationField', 'EmployeeCount', 'EmployeeNumber', 'EnvironmentSatisfaction', 'Gender', 'HourlyRate', 'JobInvolvement', 'JobLevel', 'JobRole', 'JobSatisfaction', 'MaritalStatus', 'MonthlyIncome', 'MonthlyRate', 'NumCompaniesWorked', 'Over18', 'OverTime', 'PercentSalaryHike', 'PerformanceRating', 'RelationshipSatisfaction', 'StandardHours', 'StockOptionLevel', 'TotalWorkingYears', 'TrainingTimesLastYear', 'WorkLifeBalance', 'YearsAtCompany', 'YearsInCurrentRole', 'YearsSinceLastPromotion', 'YearsWithCurrManager'
];

export interface Data {
    Age: number;
    Attrition: string;
    BusinessTravel: string;
    DailyRate: number;
    Department: string;
    DistanceFromHome: number;
    Education: number;
    EducationField: string;
    EmployeeCount: number;
    EmployeeNumber: number;
    EnvironmentSatisfaction: number;
    Gender: string;
    HourlyRate: number;
    JobInvolvement: number;
    JobLevel: number;
    JobRole: string;
    JobSatisfaction: number;
    MaritalStatus: string;
    MonthlyIncome: number;
    MonthlyRate: number;
    NumCompaniesWorked: number;
    Over18: string;
    OverTime: string;
    PercentSalaryHike: number;
    PerformanceRating: number;
    RelationshipSatisfaction: number;
    StandardHours: number;
    StockOptionLevel: number;
    TotalWorkingYears: number;
    TrainingTimesLastYear: number;
    WorkLifeBalance: number;
    YearsAtCompany: number;
    YearsInCurrentRole: number;
    YearsSinceLastPromotion: number;
    YearsWithCurrManager: number;
    [key: string]: number | string;     // so we can access it by string
}

ReactDOM.render(
    <div>
        <h1>Employee Attrition</h1>
        <App />
    </div>,
    document.getElementById( 'Root' )
);