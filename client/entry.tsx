import * as React from 'react';

interface EntryProps {
    onEnterPress: (query: string) => void;
}

interface EntryState {

}

class Entry extends React.Component <EntryProps, EntryState> {

    constructor( props: EntryProps ) {
        super( props );
        this.keyPress = this.keyPress.bind( this );
        this.searchClick = this.searchClick.bind( this );
    }

    search() {
        let input = this.refs.inputElement as HTMLInputElement;
        let query = input.value;

        this.props.onEnterPress( query );
    }

    keyPress( event: React.KeyboardEvent <HTMLInputElement> ) {
        switch( event.which ) {
            case 13:    // 'enter' key
                this.search();
                break;
        }
    }

    searchClick() {
        this.search();
    }

    render() {
        return (
            <div>
                <input ref="inputElement" type="text" placeholder="Search.." onKeyPress= { this.keyPress } />
                <button onClick= { this.searchClick }>Search</button>
            </div>

        );
    }
}

export default Entry;