# [Try it out here!](https://employee-attrition.herokuapp.com/) #

Search formats.

- `""`: When searching with the empty string, show all the elements.
- `"Property:Value"`: Filter based on the given property/value pair.

# Commands #


- `npm start` : Run the development server/build.
- `npm run releaseBuild` : Run the release build.
- `git push heroku master` : Push to server.
- `heroku run (command)` : Run commands on heroku.

Load CSV data to the database.

- `python manage.py shell`
- `exec( open( 'load_data.py' ).read() )`
